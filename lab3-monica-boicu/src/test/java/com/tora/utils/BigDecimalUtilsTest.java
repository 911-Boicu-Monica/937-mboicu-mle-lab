package com.tora.utils;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class BigDecimalUtilsTest {

    private BigDecimalUtils utils;

    @Before
    public void setup() {
        utils = new BigDecimalUtils();
    }

    @Test
    public void sumEmptyList() {
        List<BigDecimal> list = List.of();
        BigDecimal result = utils.sum(list);
        assertEquals(BigDecimal.ZERO, result);
    }

    @Test
    public void sumOneElem() {
        List<BigDecimal> list = List.of(new BigDecimal(5));
        BigDecimal result = utils.sum(list);
        assertEquals(new BigDecimal(5), result);
    }

    @Test
    public void sumTwoElems() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal("9123876723483473489246734700"));
        BigDecimal result = utils.sum(list);
        assertEquals(new BigDecimal("9123876723483473489246734705"), result);
    }

    @Test
    public void sumMultipleElems() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal("9123876723483473489246734700"), new BigDecimal(5), new BigDecimal(4));
        BigDecimal result = utils.sum(list);
        assertEquals(new BigDecimal("9123876723483473489246734714"), result);
    }

    @Test
    public void averageEmptyList() {
        List<BigDecimal> list = List.of();
        BigDecimal result = utils.average(list);
        assertEquals(BigDecimal.ZERO, result);
    }

    @Test
    public void averageOneElem() {
        List<BigDecimal> list = List.of(new BigDecimal(5));
        BigDecimal result = utils.average(list);
        assertEquals(new BigDecimal(5), result);
    }

    @Test
    public void averageTwoElems() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(7));
        BigDecimal result = utils.average(list);
        assertEquals(new BigDecimal(6), result);
    }

    @Test
    public void averageMultipleElems() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(7),  new BigDecimal(7), new BigDecimal(3));
        BigDecimal result = utils.average(list);
        assertEquals(new BigDecimal(5.5), result);
    }

    @Test
    public void topTenEmptyList() {
        List<BigDecimal> list = List.of();
        List<BigDecimal> result = utils.topTen(list);
        assertTrue(result.isEmpty());
    }

    @Test
    public void topTenOneElem() {
        List<BigDecimal> list = List.of(new BigDecimal(5));
        List<BigDecimal> result = utils.topTen(list);
        assertTrue(result.isEmpty());
    }

    @Test
    public void topTen5Elem() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5));
        List<BigDecimal> result = utils.topTen(list);
        assertEquals(1, result.size());
        assertThat(result, Matchers.contains(new BigDecimal(10)));
    }

    @Test
    public void topTen10Elem() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(8), new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5));
        List<BigDecimal> result = utils.topTen(list);
        assertEquals(1, result.size());
        assertThat(result, Matchers.contains(new BigDecimal(10)));
    }

    @Test
    public void topTen20Elem() {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(8), new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5),
                new BigDecimal(5), new BigDecimal(14), new BigDecimal(5), new BigDecimal(5), new BigDecimal(8), new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5));
        List<BigDecimal> result = utils.topTen(list);
        assertEquals(2, result.size());
        assertThat(result, Matchers.containsInRelativeOrder(new BigDecimal(14), new BigDecimal(10)));
    }
}