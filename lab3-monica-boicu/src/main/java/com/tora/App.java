package com.tora;

import com.tora.utils.BigDecimalUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        List<BigDecimal> list = List.of(new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5), new BigDecimal(8), new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5),
                new BigDecimal(5), new BigDecimal(14), new BigDecimal(5), new BigDecimal(5), new BigDecimal(8), new BigDecimal(5), new BigDecimal(10), new BigDecimal(5), new BigDecimal(5), new BigDecimal(5));
        BigDecimalUtils utils = new BigDecimalUtils();

        System.out.println("Sum = " + utils.sum(list));
        System.out.println("Average = " + utils.average(list));
        System.out.println("Top 10% : " + utils.topTen(list));

        String bigDecimalsFilePath = App.class.getClassLoader().getResource("data.txt").getPath();
        utils.serialize(list, bigDecimalsFilePath);
        List<BigDecimal> deserialized = utils.deserialize(bigDecimalsFilePath);
        System.out.println("Before serialize:" + list);
        System.out.println("Deserialized:    " + deserialized);

        String lambdasFilePath = App.class.getClassLoader().getResource("lambda.txt").getPath();

        utils.serializeLambdas(lambdasFilePath);
        List<BiFunction> deserializedFunctions = utils.deserialize(lambdasFilePath);
        System.out.println("Result for deserialized sum lambda = " + deserializedFunctions.get(0).apply(utils, list));
        System.out.println("Result for deserialized average lambda = " + deserializedFunctions.get(1).apply(utils, list));
        System.out.println("Result for deserialized top 10% lambda = " + deserializedFunctions.get(2).apply(utils, list));
    }
}
