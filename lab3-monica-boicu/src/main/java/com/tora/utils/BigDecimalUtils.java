package com.tora.utils;

import com.tora.utils.exception.SerializationException;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class BigDecimalUtils {

    public BigDecimal sum(List<BigDecimal> list) {
        return list.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average(List<BigDecimal> list) {
        if (list.isEmpty()) {
            return BigDecimal.ZERO;
        }
        return list.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(list.size()));
    }

    public List<BigDecimal> topTen(List<BigDecimal> list) {
        if (list.isEmpty()) {
            return List.of();
        }
        int tenPercentCount = (int) Math.round(list.size() * 0.1);
        return list.stream()
                .sorted(Comparator.reverseOrder())
                .limit(tenPercentCount)
                .collect(Collectors.toList());
    }

    public <T> void serialize(List<T> list, String filePath) {
        try (FileOutputStream fos = new FileOutputStream(filePath);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            list.forEach(item -> {
                try {
                    oos.writeObject(item);
                } catch (IOException e) {
                    throw new SerializationException("Failed to serialize values.", e);
                }
            });
            oos.writeObject(null);
        } catch (IOException e) {
            throw new SerializationException("Failed to serialize values.", e);
        }
    }

    public <T> List<T> deserialize(String filePath) {
        List<T> result = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(filePath);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            T value;
            while ((value = (T) ois.readObject()) != null) {
                result.add(value);
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new SerializationException("Failed to deserialize values.", e);
        }
        return result;
    }

    public void serializeLambdas(String filePath) {
        serialize(List.of(
                (BiFunction<BigDecimalUtils, List<BigDecimal>, BigDecimal> & Serializable) (bdUtils, bdList) -> bdUtils.sum(bdList),
                (BiFunction<BigDecimalUtils, List<BigDecimal>, BigDecimal> & Serializable) (bdUtils, bdList) -> bdUtils.average(bdList),
                (BiFunction<BigDecimalUtils, List<BigDecimal>, List<BigDecimal>> & Serializable) (bdUtils, bdList) -> bdUtils.topTen(bdList)
        ), filePath);
    }

}
