package com.tora.evaluator.impl;

import com.tora.exception.FormulaFormatException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EvalExFormulaEvaluatorTest {

    private EvalExFormulaEvaluator evaluator;

    @Before
    public void init() {
        evaluator = new EvalExFormulaEvaluator();
    }

    @Test
    public void evaluateSumWithoutWhiteSpaces() {
        String result = evaluator.evaluate("5+5");
        assertEquals("10", result);
    }

    @Test
    public void evaluateSumWithWhiteSpaces() {
        String result = evaluator.evaluate("5+ 5+ 2 +       7");
        assertEquals("19", result);
    }

    @Test
    public void evaluateDiff() {
        String result = evaluator.evaluate("5- 5- 2 -       7");
        assertEquals("-9", result);
    }

    @Test
    public void evaluateProduct() {
        String result = evaluator.evaluate("5* 2 *       7");
        assertEquals("70", result);
    }

    @Test
    public void evaluateDiv() {
        String result = evaluator.evaluate("9/ 2    /4.5");
        assertEquals("1", result);
    }

    @Test
    public void evaluateMultipleOperatorsFormula() {
        String result = evaluator.evaluate("9/ 2 *       (4+4 - 1)");
        assertEquals("31.5", result);
    }

    @Test
    public void evaluateSqrt() {
        String result = evaluator.evaluate("sqrt( 4)");
        assertEquals("2", result);
    }

    @Test
    public void evaluateMin() {
        String result = evaluator.evaluate("min( 4, -5, 200, -250)");
        assertEquals("-250", result);
    }

    @Test
    public void evaluateMax() {
        String result = evaluator.evaluate("max( 4, -5, 200, -250)");
        assertEquals("200", result);
    }

    @Test
    public void evaluateWrongFormulaException() {
        assertThrows(FormulaFormatException.class, () -> evaluator.evaluate("asja"));
    }
}