package com.tora.evaluator.impl;

import com.ezylang.evalex.EvaluationException;
import com.ezylang.evalex.Expression;
import com.ezylang.evalex.data.EvaluationValue;
import com.ezylang.evalex.parser.ParseException;
import com.tora.evaluator.FormulaEvaluator;
import com.tora.exception.FormulaFormatException;

public class EvalExFormulaEvaluator implements FormulaEvaluator {

    @Override
    public String evaluate(String formula) {
        Expression expression = new Expression(formula);
        try {
            EvaluationValue result = expression.evaluate();
            return result.getStringValue();
        } catch (EvaluationException | ParseException e) {
            throw new FormulaFormatException("Invalid formula " + formula, e);
        }
    }
}
