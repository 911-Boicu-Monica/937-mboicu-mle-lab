package com.tora.evaluator;

public interface FormulaEvaluator {

    String evaluate(String formula);
}
