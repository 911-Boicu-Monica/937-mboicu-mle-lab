package com.tora.exception;

public class FormulaFormatException extends RuntimeException {

    public FormulaFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
