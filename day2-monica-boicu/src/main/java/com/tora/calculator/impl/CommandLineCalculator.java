package com.tora.calculator.impl;

import com.tora.calculator.Calculator;
import com.tora.evaluator.FormulaEvaluator;
import com.tora.exception.FormulaFormatException;

import java.util.Scanner;

public class CommandLineCalculator implements Calculator {

    public static final String EXIT_COMMAND = "exit";

    private final FormulaEvaluator formulaEvaluator;

    public CommandLineCalculator(FormulaEvaluator formulaEvaluator) {
        this.formulaEvaluator = formulaEvaluator;
    }

    @Override
    public void start() {
        System.out.println("Command line calculator switched on.");
        try (Scanner scanner = new Scanner(System.in)) {
            while (!scanner.hasNext(EXIT_COMMAND)) {
                String formula = scanner.nextLine();
                try {
                    String result = formulaEvaluator.evaluate(formula);
                    System.out.printf("%s = %s%n", formula, result);
                } catch (FormulaFormatException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
