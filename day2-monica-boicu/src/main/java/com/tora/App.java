package com.tora;

import com.tora.calculator.Calculator;
import com.tora.calculator.impl.CommandLineCalculator;
import com.tora.evaluator.FormulaEvaluator;
import com.tora.evaluator.impl.EvalExFormulaEvaluator;

/**
 * Main class for calculator App
 */
public class App {
    public static void main(String[] args) {
        FormulaEvaluator formulaEvaluator = new EvalExFormulaEvaluator();
        Calculator calculator = new CommandLineCalculator(formulaEvaluator);
        calculator.start();
    }
}
