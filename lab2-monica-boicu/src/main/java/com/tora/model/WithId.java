package com.tora.model;

public interface WithId {

    int getId();
}
