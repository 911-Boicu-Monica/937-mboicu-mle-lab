package com.tora.model.repository.impl;

import com.tora.model.WithId;
import com.tora.model.repository.InMemoryRepository;
import gnu.trove.map.hash.TIntObjectHashMap;

public class Tove4jTIntObjectHashMapBasedRepository<T extends WithId> implements InMemoryRepository<T> {

    private TIntObjectHashMap<T> map = new TIntObjectHashMap<>();

    @Override
    public void add(T entity) {
        map.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.contains(entity.getId());
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }
}
