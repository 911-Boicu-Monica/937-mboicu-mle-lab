package com.tora.model.repository.impl;

import com.tora.model.WithId;
import com.tora.model.repository.InMemoryRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T extends WithId> implements InMemoryRepository<T> {

    private Map<Integer, T> map = new ConcurrentHashMap<>();

    @Override
    public void add(T entity) {
        map.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId());
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }
}
