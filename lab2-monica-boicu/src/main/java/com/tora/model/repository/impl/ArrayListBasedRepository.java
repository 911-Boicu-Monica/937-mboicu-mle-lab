package com.tora.model.repository.impl;

import com.tora.model.repository.InMemoryRepository;
import org.openjdk.jmh.annotations.Benchmark;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> list = new ArrayList<>();

    @Override
    public void add(T entity) {
        if (!list.contains(entity)) {
            list.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        return list.contains(entity);
    }

    @Override
    public void remove(T entity) {
        list.remove(entity);
    }
}
