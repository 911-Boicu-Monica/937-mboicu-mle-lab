package com.tora.model.repository.impl;


import com.koloboke.collect.set.hash.HashObjSets;
import com.tora.model.repository.InMemoryRepository;

import java.util.Set;

public class KolobokeObjSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set = HashObjSets.getDefaultFactory().newUpdatableSet();

    @Override
    public void add(T entity) {
        if (!set.contains(entity)) {
            set.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }
}
