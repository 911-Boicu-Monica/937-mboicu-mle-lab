package com.tora.model.repository.impl;

import com.tora.model.repository.InMemoryRepository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set = new HashSet<T>();

    @Override
    public void add(T entity) {
        if (!set.contains(entity)) {
            set.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }
}
