package com.tora.model.repository;

public interface InMemoryRepository<T> {
    void add(T entity);

    boolean contains(T entity);

    void remove(T entity);
}
