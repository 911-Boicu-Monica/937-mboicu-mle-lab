package com.tora.model.repository.impl;

import com.tora.model.repository.InMemoryRepository;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set;

    public TreeSetBasedRepository(Comparator<T> comparator) {
        set = new TreeSet<>(comparator);
    }

    @Override
    public void add(T entity) {
        if (!set.contains(entity)) {
            set.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }
}
