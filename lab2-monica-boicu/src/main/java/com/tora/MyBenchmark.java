/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.tora;

import com.tora.model.Order;
import com.tora.model.repository.InMemoryRepository;
import com.tora.model.repository.impl.*;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Comparator;


public class MyBenchmark {


    public static abstract class AbstractState {
        public Order orderForAdd = new Order(6,5,6);
        public Order orderForRemove = new Order(6,5,6);
        public Order orderForContains = new Order(6,5,6);
        protected abstract InMemoryRepository<Order> getRepository();

        @Setup
        public void setup() {
            getRepository().add(new Order(1,5,6));
            getRepository().add(new Order(3,5,6));
            getRepository().add(new Order(2,5,6));
            getRepository().add(new Order(5,5,6));
            getRepository().add(new Order(4,5,6));
            getRepository().add(new Order(7,5,6));
        }
    }

    @State(Scope.Benchmark)
    public static class ArrayListBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new ArrayListBasedRepository<> ();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new ConcurrentHashMapBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class EclipseFastListBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new EclipseFastListBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class FastUtilObjectArrayListBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new FastUtilObjectArrayListBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new HashSetBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class KolobokeObjSetBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new KolobokeObjSetBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class Tove4jTIntObjectHashMapBasedRepositoryState extends AbstractState {

        public InMemoryRepository<Order> repository = new Tove4jTIntObjectHashMapBasedRepository<>();
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetBasedRepositoryState extends AbstractState {

        public Comparator<Order> comp = (Order o1, Order o2) -> (Integer.compare(o1.getId(), o2.getId()));
        public InMemoryRepository<Order> repository = new TreeSetBasedRepository<>(comp);
        @Override
        protected InMemoryRepository<Order> getRepository() {
            return this.repository;
        }
    }

    @Benchmark
    public void add_ArrayListBasedRepository(ArrayListBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_ArrayListBasedRepository(ArrayListBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_ArrayListBasedRepository(ArrayListBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_ConcurrentHashMapBasedRepository(ConcurrentHashMapBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_ConcurrentHashMapBasedRepository(ConcurrentHashMapBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_ConcurrentHashMapBasedRepository(ConcurrentHashMapBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_EclipseFastListBasedRepository(EclipseFastListBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_EclipseFastListBasedRepository(EclipseFastListBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_EclipseFastListBasedRepository(EclipseFastListBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_FastUtilObjectArrayListBasedRepository(FastUtilObjectArrayListBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_FastUtilObjectArrayListBasedRepository(FastUtilObjectArrayListBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_FastUtilObjectArrayListBasedRepository(FastUtilObjectArrayListBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_HashSetBasedRepository(HashSetBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_HashSetBasedRepository(HashSetBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_HashSetBasedRepository(HashSetBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_Tove4jTIntObjectHashMapBasedRepository(Tove4jTIntObjectHashMapBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_Tove4jTIntObjectHashMapBasedRepository(Tove4jTIntObjectHashMapBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_Tove4jTIntObjectHashMapBasedRepository(Tove4jTIntObjectHashMapBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    @Benchmark
    public void add_TreeSetBasedRepository(TreeSetBasedRepositoryState state) {
        state.getRepository().add(state.orderForAdd);
    }

    @Benchmark
    public void remove_TreeSetBasedRepository(TreeSetBasedRepositoryState state) {
        state.getRepository().remove(state.orderForRemove);
    }

    @Benchmark
    public void contains_TreeSetBasedRepository(TreeSetBasedRepositoryState state, Blackhole blackhole) {
        boolean result = state.getRepository().contains(state.orderForContains);
        blackhole.consume(result);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
//                .addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}
